﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ServiceModel;
using System.Xml;

namespace WebApplication1 {
    public partial class _Default : Page {

        private Uri baseUri = new Uri("http://localhost:49274/Service1.svc/");
        private UriTemplate GetSecretNumTemplate = new UriTemplate("SecretNumber?l={l}&u={u}");
        private UriTemplate GetGuessTemplate = new UriTemplate("checkNumber?cNum={cNum}&sNum={sNum}");
        private int SecretNum = -1;
        private string SecretNumViewState = "SecretNum";
        private int GuessAttempts;
        private string GuessAttemptsViewState = "GuessAttempts";

        HttpWebRequest request;
        WebResponse response;
        XmlDocument doc;

        protected void Page_Load(object sender, EventArgs e) {
            Lbl_GenResponse.Text = "";
        }

        protected void Btn_GenerateSN_Click(object sender, EventArgs e) {
            int LowerLimit, UpperLimit;

            if (GetLimits(out LowerLimit, out UpperLimit)) {

                Uri getSecretNumUri = GetSecretNumTemplate.BindByPosition(baseUri, LowerLimit.ToString(), UpperLimit.ToString());

                try {
                    request = (HttpWebRequest)WebRequest.Create(getSecretNumUri);
                    response = request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader sreader = new StreamReader(dataStream);
                    string responseReader = sreader.ReadToEnd();
                    response.Close();

                    doc = new XmlDocument();
                    doc.LoadXml(responseReader);
                    SecretNum = GetSecretNum(doc);
                    ResetGuessAttempts();
                }
                catch (WebException ex) {
                    Lbl_GenResponse.Text = ex.Message.ToString();
                }

                // Lbl_GenResponse.Text = SecretNum.ToString();
                SetViewStateSecretNum(SecretNum.ToString());
            }
            else {
                Lbl_GenResponse.Text = "Error: Invalid input numbers";
            }
        }

        private bool GetLimits(out int lower, out int upper) {
            string rawInputLowerLimit = TB_LowerLimit.Text;
            string rawInputupperLimit = TB_UpperLimit.Text;

            if (rawInputLowerLimit.Length < 1 || rawInputupperLimit.Length < 1) {
                upper = 0;
                lower = 0;
                return false;
            }

            if (ValidateDigitInput(rawInputLowerLimit) && ValidateDigitInput(rawInputupperLimit)) {
                lower = ConvertToInt(rawInputLowerLimit);
                upper = ConvertToInt(rawInputupperLimit);

                if (lower <= upper)
                    return true;
                else
                    return false;
            }

            upper = 0;
            lower = 0;
            return false;
        }

        private bool ValidateDigitInput(string input) {
            foreach (char ch in input) {
                if (!Char.IsDigit(ch))
                    return false;
            }
            return true;
        }

        private int ConvertToInt(string input) {
            int newInt = 0;

            try {
                newInt = int.Parse(input);
            }
            catch (ArgumentNullException) {
                Lbl_GenResponse.Text = "Invalid Input - input must be numbers";
            }
            catch (FormatException) {
                Lbl_GenResponse.Text = "Invalid Input - input must be numbers";
            }
            catch (OverflowException) {
                Lbl_GenResponse.Text = "Input number is too large - try a smaller number.";
            }

            return newInt;
        }

        private int GetSecretNum(XmlDocument doc) {
            string value = doc.FirstChild.InnerText;
            int intVal;

            if (int.TryParse(value, out intVal))
                return intVal;
            else
                return 0;
        }

        private void SetViewStateSecretNum(string secretNum) {
            ViewState[SecretNumViewState] = secretNum;
        }

        private void ClearViewStateSecretNum() {
            ViewState[SecretNumViewState] = null;
        }

        private string GetViewStateSecretNum() {
            return ViewState[SecretNumViewState].ToString();
        }

        private void ResetGuessAttempts() {
            ViewState[GuessAttemptsViewState] = "0";
            Lbl_Attempts.Text = "0";
        }

        protected void Btn_Play_Click(object sender, EventArgs e) {
            string guess = TB_Guess.Text;
            Lbl_Response.Text = "";

            if (guess != null && ValidateDigitInput(guess) && guess.Length > 0) {
                Uri playUri = GetGuessTemplate.BindByPosition(baseUri, guess, GetViewStateSecretNum());

                try {
                    request = (HttpWebRequest)WebRequest.Create(playUri);
                    response = request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader sreader = new StreamReader(dataStream);
                    string responseReader = sreader.ReadToEnd();
                    response.Close();

                    doc = new XmlDocument();
                    doc.LoadXml(responseReader);
                    Lbl_Response.Text = GetGuessResponse(doc);
                    
                    IncrementGuessAttempts();
                }
                catch (WebException ex) {
                    Lbl_Response.Text = ex.Message.ToString();
                }
            }
            else {
                Lbl_Response.Text = "Invalid guess - you must enter a number.";
            }
        }

        private string GetGuessResponse(XmlDocument doc) {
            return doc.FirstChild.InnerText;
        }

        private void IncrementGuessAttempts() {
            int result;
            if (int.TryParse(ViewState[GuessAttemptsViewState].ToString(), out result)) {
                result++;
                ViewState[GuessAttemptsViewState] = result.ToString();
                Lbl_Attempts.Text = result.ToString();
            }
            else {
                ViewState[GuessAttemptsViewState] = "0";
                Lbl_Attempts.Text = "0";
            }

        }
    }
}
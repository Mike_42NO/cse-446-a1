﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Welcome to Number Guessing Game!</h1>
        <h2>Michael Fortuno - CSE 446 - Spring 2015 - A1 P2 Q6</h2>
        <p>
            <asp:Label ID="Label1" runat="server" Text="Lower Limit"></asp:Label>&nbsp;
            <asp:TextBox ID="TB_LowerLimit" runat="server"></asp:TextBox>&nbsp;
            <asp:Label ID="Label2" runat="server" Text="Upper Limit"></asp:Label>&nbsp;
            <asp:TextBox ID="TB_UpperLimit" runat="server"></asp:TextBox>&nbsp;
            <asp:Button ID="Btn_GenerateSN" runat="server" Text="Generate a Secret Number" OnClick="Btn_GenerateSN_Click" />&nbsp;
            <asp:Label ID="Lbl_GenResponse" runat="server" Text=""></asp:Label>
        </p>
        <p>
            <asp:Label ID="Label3" runat="server" Text="Make a Guess"></asp:Label>&nbsp;
            <asp:TextBox ID="TB_Guess" runat="server"></asp:TextBox>&nbsp;<asp:Button ID="Btn_Play" runat="server" OnClick="Btn_Play_Click" Text="Play" />
&nbsp;<asp:Label ID="Label4" runat="server" Text="Attempts"></asp:Label>&nbsp;
            <asp:Label ID="Lbl_Attempts" runat="server" Text="-"></asp:Label>&nbsp;
            <asp:Label ID="Label5" runat="server" Text="The number is"></asp:Label>&nbsp;
            <asp:Label ID="Lbl_Response" runat="server" Text=""></asp:Label>&nbsp;


        </p>
    </div>

    </div>

</asp:Content>

﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_EnterString = new System.Windows.Forms.Label();
            this.tb_Verify = new System.Windows.Forms.TextBox();
            this.btn_Verify = new System.Windows.Forms.Button();
            this.lbl_VerificationResult = new System.Windows.Forms.Label();
            this.btn_CallAndVerify = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_EnterString
            // 
            this.lbl_EnterString.AutoSize = true;
            this.lbl_EnterString.Location = new System.Drawing.Point(13, 44);
            this.lbl_EnterString.Name = "lbl_EnterString";
            this.lbl_EnterString.Size = new System.Drawing.Size(69, 13);
            this.lbl_EnterString.TabIndex = 0;
            this.lbl_EnterString.Text = "Enter a string";
            // 
            // tb_Verify
            // 
            this.tb_Verify.Location = new System.Drawing.Point(89, 44);
            this.tb_Verify.Name = "tb_Verify";
            this.tb_Verify.Size = new System.Drawing.Size(100, 20);
            this.tb_Verify.TabIndex = 1;
            // 
            // btn_Verify
            // 
            this.btn_Verify.Location = new System.Drawing.Point(196, 44);
            this.btn_Verify.Name = "btn_Verify";
            this.btn_Verify.Size = new System.Drawing.Size(163, 23);
            this.btn_Verify.TabIndex = 2;
            this.btn_Verify.Text = "Verify the entered string";
            this.btn_Verify.UseVisualStyleBackColor = true;
            this.btn_Verify.Click += new System.EventHandler(this.btn_Verify_Click);
            // 
            // lbl_VerificationResult
            // 
            this.lbl_VerificationResult.AutoSize = true;
            this.lbl_VerificationResult.Location = new System.Drawing.Point(365, 47);
            this.lbl_VerificationResult.Name = "lbl_VerificationResult";
            this.lbl_VerificationResult.Size = new System.Drawing.Size(92, 13);
            this.lbl_VerificationResult.TabIndex = 3;
            this.lbl_VerificationResult.Text = "Verification Result";
            // 
            // btn_CallAndVerify
            // 
            this.btn_CallAndVerify.Location = new System.Drawing.Point(196, 73);
            this.btn_CallAndVerify.Name = "btn_CallAndVerify";
            this.btn_CallAndVerify.Size = new System.Drawing.Size(163, 23);
            this.btn_CallAndVerify.TabIndex = 4;
            this.btn_CallAndVerify.Text = "Call and verify service output";
            this.btn_CallAndVerify.UseVisualStyleBackColor = true;
            this.btn_CallAndVerify.Click += new System.EventHandler(this.btn_CallAndVerify_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "A1 P1 - Strong password verification application";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 114);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_CallAndVerify);
            this.Controls.Add(this.lbl_VerificationResult);
            this.Controls.Add(this.btn_Verify);
            this.Controls.Add(this.tb_Verify);
            this.Controls.Add(this.lbl_EnterString);
            this.Name = "Form1";
            this.Text = "Michael Fortuno - CSE 446 - A1 P1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_EnterString;
        private System.Windows.Forms.TextBox tb_Verify;
        private System.Windows.Forms.Button btn_Verify;
        private System.Windows.Forms.Label lbl_VerificationResult;
        private System.Windows.Forms.Button btn_CallAndVerify;
        private System.Windows.Forms.Label label1;
    }
}


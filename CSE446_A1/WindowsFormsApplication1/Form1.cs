﻿/**
 * Michael Fortuno 
 * CSE 446 - Spring 2015 - Chen
 * Assignment 1 Part 1
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSE446_A1;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private int MinimumPasswordLength = 6;

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Verify_Click(object sender, EventArgs e)
        {
            string password = tb_Verify.Text;
            Service1 StrongPasswordSvc = new Service1();

            bool isStrongPassword = StrongPasswordSvc.IsStrongPassword(password);
            if (isStrongPassword)
                lbl_VerificationResult.Text = "True";
            else
                lbl_VerificationResult.Text = "False";
        }

        private void btn_CallAndVerify_Click(object sender, EventArgs e)
        {
            Service1 StrongPasswordSvc = new Service1();

            string NewPassword = StrongPasswordSvc.GeneratePassword(MinimumPasswordLength);
            tb_Verify.Text = NewPassword;
            btn_Verify.PerformClick();
        }
    }
}

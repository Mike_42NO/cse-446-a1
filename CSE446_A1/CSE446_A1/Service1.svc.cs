﻿/**
 * Michael Fortuno 
 * CSE 446 - Spring 2015 - Chen
 * Assignment 1 Part 1
 **/

using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CSE446_A1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private int MinimumPwLength = 6;
        
        private string SpecialChars = "!@#$%^&*()";

        Random r = new Random();
        private int AsciiLowerStart = 97;   // inclusive
        private int AsciiLowerEnd = 123;    // exclusive

        public string GeneratePassword(int passwordLength)
        {
            if (!IsMinimumLength(passwordLength)) {
                return "Error: password must be at least " + MinimumPwLength + " characters.";
            }

            string orderedPassword = GenerateOrderedPassword(passwordLength);

            return ReorderedPassword(orderedPassword);
        }

        private bool IsMinimumLength(int l)
        {
            if (l < MinimumPwLength)
                return false;

            return true;
        }

        private bool IsMinimumLength(string str)
        {
            if (str.Length < MinimumPwLength)
                return false;

            return true;
        }

        private string GenerateOrderedPassword(int pwLength)
        {
            StringBuilder sb = new StringBuilder();

            for (var i = 0; i < pwLength; i++)
                sb.Append(GetNextToken(i));

            return sb.ToString();
        }

        private string ReorderedPassword(string password)
        {
            StringBuilder sb = new StringBuilder();

            for (var i = 0; i < password.Length; i++ )
            {
                var insertIndex = r.Next(0, sb.Length);
                sb.Insert(insertIndex, password[i]);
            }

            return sb.ToString();
        }

        private char GetNextToken(int i) {
            switch (i)
            {
                case 0:
                    return GetRandomLowerChar();
                case 1:
                    return GetRandomUpperChar();
                case 2:
                    return GetRandomInteger();
                case 3:
                    return GetRandomSpecialCharacter();
                default:
                    return GetNextToken(r.Next(0,4));
            }
        }

        private char GetRandomLowerChar() {
            int num = r.Next(AsciiLowerStart, AsciiLowerEnd);
            return Convert.ToChar(num);
        }

        private char GetRandomUpperChar() {
            return Char.ToUpper(GetRandomLowerChar());
        }

        private char GetRandomInteger(){
            int num = r.Next(0, 10);
            string str = num.ToString();
            return Convert.ToChar(str[0]);
        }

        private char GetRandomSpecialCharacter() {
            int num = r.Next(0, SpecialChars.Length);
            char c;

            try {
                c = SpecialChars[num];
            }
            catch (IndexOutOfRangeException) {
                return SpecialChars[0];
            }

            return c;
        }

        private string TestAsciiChars(int start, int end) {
            StringBuilder sb = new StringBuilder();

            for (int i = start; i < end; i++)
            {
                sb.Append(Convert.ToChar(i));
            }

            return sb.ToString();
        }

        public bool IsStrongPassword(string password)
        {
            if (!IsMinimumLength(password))
                return false;

            bool hasLowerChar = HasLowerChar(password);
            bool hasUpperChar = HasUpperChar(password);
            bool hasInteger = HasInteger(password);
            bool hasSpecialChar = HasSpecialChar(password);

            return (hasLowerChar && hasUpperChar && hasInteger && hasSpecialChar);
        }

        private bool HasLowerChar(string str)
        {
            foreach(char ch in str)
            {
                if (Char.IsLower(ch))
                    return true;
            }
            return false;
        }

        private bool HasUpperChar(string str)
        {
            foreach (char ch in str)
            {
                if (Char.IsUpper(ch))
                    return true;
            }
            return false;
        }

        private bool HasInteger(string str)
        {
            foreach(char ch in str)
            {
                if (Char.IsDigit(ch))
                    return true;
            }
            return false;
        }

        private bool HasSpecialChar(string str)
        {
            foreach(char ch in str)
            {
                for(var i = 0; i < SpecialChars.Length; i++)
                {
                    if (ch == SpecialChars[i])
                        return true;
                }
            }
            return false;
        }
    }
}

﻿/**
 * Michael Fortuno 
 * CSE 446 - Spring 2015 - Chen
 * Assignment 1 Part 1
 **/

using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CSE446_A1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string GeneratePassword(int passwordLength);

        [OperationContract]
        bool IsStrongPassword(string password);
    }

}

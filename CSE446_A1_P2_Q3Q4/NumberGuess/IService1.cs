﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace NumberGuess {

    [ServiceContract]
    public interface INumberGuess {

        [OperationContract]
        [WebGet(UriTemplate = "SecretNumberPlz?lower={l}&upper={u}")]
        int SecretNumber(int l, int u);

        [OperationContract]
        [WebGet(UriTemplate = "DidIWin?guess={cNum}&notTheSecret={sNum}")]
        string checkNumber(int cNum, int sNum);
    }
}

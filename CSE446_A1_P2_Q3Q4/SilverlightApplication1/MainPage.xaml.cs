﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace SilverlightApplication1 {
    public partial class MainPage : UserControl {
        public MainPage() {
            InitializeComponent();
            Rect_Red.Margin = new Thickness(-640, 0, 0, 0);
            Lbl_Info.Content = "CSE 446 - A1 P2 Q4 - Michael Fortuno";
        }

        private string SecretNumRestSvcUrl = "http://localhost:49274/Service1.svc/";
        private string GetSecretNumTemplate = "SecretNumberPlz?lower={l}&upper={u}";
        private string CheckAnswerTemplate = "DidIWin?guess={cNum}&notTheSecret={sNum}";
        private int secretNum;
        private bool HaveSecretNumber = false;
        private int GuessAttempts = 0;

        private double AmountToMoveVictim = 75.0;

        private void Btn_GenSN_Click(object sender, RoutedEventArgs e) {
            string lowerLimit = TB_LowerLimit.Text;
            string upperLimit = TB_UpperLimit.Text;

            Uri GetSecretNumUri = BuildGetUri(lowerLimit, upperLimit);
            //Lbl_Info.Content = GetSecretNumUri.ToString();

            WebClient client = new WebClient();
            string myNumber = "";

            client.DownloadStringCompleted += (s, ev) => {
                XDocument xml = XDocument.Parse(ev.Result);
                myNumber = xml.Root.Value.ToString();

                SetSecretNumber(Int32.Parse(myNumber));
                Lbl_Info.Content = "Ok, I have the number >:)";
            };
            client.DownloadStringAsync(GetSecretNumUri);

        }

        private Uri BuildGetUri(string lower, string upper) {
            string newUri = SecretNumRestSvcUrl;
            newUri += GetSecretNumTemplate;
            
            string temp = newUri.Replace("{l}", lower);
            temp = temp.Replace("{u}", upper);

            return new Uri(temp);
        }

        private void SetSecretNumber(int sNum) {
            secretNum = sNum;
            HaveSecretNumber = true;
        }

        private void Btn_Play_Click(object sender, RoutedEventArgs e) {
            if (!HaveSecretNumber) {
                Lbl_GuessResponse.Content = "Generate a secret number above before you guess.";
                return;
            }

            Lbl_Info.Content = "";

            string guess = Tb_Guess.Text;
            WebClient client = new WebClient();
            string clientResponse = "";
            string responseMessage = "Something went wrong :(";

            Uri CheckGuessUri = BuildCheckGuess(guess);
            //Lbl_GuessResponse.Content = CheckGuessUri.ToString();

            client.DownloadStringCompleted += (s, ev) => {
                XDocument xml = XDocument.Parse(ev.Result);
                clientResponse = xml.Root.Value;

                DidPlayerWin(clientResponse);

                responseMessage = BuildResponseMessage(clientResponse);
                Lbl_GuessResponse.Content = responseMessage;
            };
            client.DownloadStringAsync(CheckGuessUri);

            

        }

        private Uri BuildCheckGuess(string guess) {
            string newUri = SecretNumRestSvcUrl;
            newUri += CheckAnswerTemplate;
            string temp = newUri.Replace("{cNum}", guess);
            temp = temp.Replace("{sNum}", secretNum.ToString());

            return new Uri(temp);
        }

        private void DidPlayerWin(string response) {
            if (response.Equals("correct")) {
                Lbl_Info.Content = "You win!";
            }
            else {
                MoveVictim.X -= AmountToMoveVictim;
                if (GuessAttempts >= 3) {
                    Rect_Red.Margin= new Thickness(0,0,0,0);
                }
            }
        }

        private string BuildResponseMessage(string clientResponse) {
            GuessAttempts++;

            string newString = "Attempts: ";
            if (!HaveSecretNumber)
                newString += "- ";
            else {
                newString += GuessAttempts.ToString();
                newString += " the guess is ";
                newString += clientResponse;
            }

            return newString;
        }
    }
}

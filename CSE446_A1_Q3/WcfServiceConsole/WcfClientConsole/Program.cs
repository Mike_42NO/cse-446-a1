﻿/**
 * Michael Fortuno
 * CSE 446 - Spring 2015
 * Assignment 1 - Part 1 - Q4
 **/

using System;
using System.ServiceModel;

namespace WcfClientConsole {
    class Program {
        private static int passwordLength = 6;

        static void Main(string[] args) {
            Console.WriteLine("Michael Fortuno - A1 P1 Q4");
            
            myInterfaceClient myProxy = new myInterfaceClient();
            string str = myProxy.GenerateStrongPassword(passwordLength);
            myProxy.Close();

            Console.WriteLine("Password recieved from self-hosted service is {0}", str);
            Console.WriteLine("\nPress ENTER to close.\n");
            Console.ReadLine();
        }
    }
}

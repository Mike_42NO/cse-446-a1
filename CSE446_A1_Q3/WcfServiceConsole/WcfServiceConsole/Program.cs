﻿/**
 * Michael Fortuno
 * CSE 446 - Spring 2015
 * Assignment 1 - Part 1 - Q3
 **/

using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using CSE446_A1;

namespace SelfHostingService {
    
    [ServiceContract]
    public interface myInterface {
        [OperationContract]
        string GenerateStrongPassword(int passwordLength);
    }

    public class myService : myInterface {
        public string GenerateStrongPassword(int pwLength) {
            CSE446_A1.Service1 pwGenerator = new Service1();
            return pwGenerator.GeneratePassword(6);
        }
    }

    class Program {
        static void Main(string[] args) {
            Uri baseAddress = new Uri("http://localhost:8000/Service");
            ServiceHost selfHost = new ServiceHost(typeof(myService), baseAddress);

            try {
                selfHost.AddServiceEndpoint(typeof(myInterface), new WSHttpBinding(), "myService");

                selfHost.Description.Behaviors.Add(CreateAndConfigureServiceBehavior());

                selfHost.Open();

                Console.WriteLine("myService is ready to take requests. Please create a client to call string GenerateStrongPassword(int).");
                Console.WriteLine("If you want to quit this service, simply press ENTER.\n");
                Console.ReadLine();
                
                selfHost.Close();
            }
            catch (CommunicationException ce) {
                Console.WriteLine("An exception occured: {0}", ce.Message);
                selfHost.Abort();
            }
        }

        private static System.ServiceModel.Description.ServiceMetadataBehavior CreateAndConfigureServiceBehavior() {
            System.ServiceModel.Description.ServiceMetadataBehavior smb = new System.ServiceModel.Description.ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            return smb;
        }
    }
}

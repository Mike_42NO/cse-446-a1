﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SelfHostingService;

namespace SelfHostingServiceTests {
    [TestClass]
    public class myServiceTests {

        private int pwLength = 6;

        [TestMethod]
        public void TestGenerateStrongPasswordLegth() {
            myService ms = new myService();
            string str = ms.GenerateStrongPassword(pwLength);
            Assert.AreEqual(str.Length, pwLength);
        }

        [TestMethod]
        public void TestGSPIncludesUpper() {
            myService ms = new myService();
            string str = ms.GenerateStrongPassword(pwLength);
            bool containsUpper = false;
            
            foreach (char ch in str)
                if (Char.IsUpper(ch))
                    containsUpper = true;

            Assert.IsTrue(containsUpper);
        }

        [TestMethod]
        public void TestGSPIncludesLower() {
            myService ms = new myService();
            string str = ms.GenerateStrongPassword(pwLength);
            bool containsLower = false;

            foreach (char ch in str)
                if (Char.IsLower(ch))
                    containsLower = true;

            Assert.IsTrue(containsLower);
        }

        [TestMethod]
        public void TestGSPIncludesInteger() {
            myService ms = new myService();
            string str = ms.GenerateStrongPassword(pwLength);
            bool containsInteger = false;

            foreach (char ch in str)
                if (Char.IsDigit(ch))
                    containsInteger = true;

            Assert.IsTrue(containsInteger);
        }
    }
}
